# React UI

## Description

This is a Vite React project for customer UI.

We have mainly three requirements for every IoT device:
1. View real-time and historical data from the device.
2. Have ability to configure the device.
3. Have ability to configure alerts which would be emitted from platform.

## Running with Docker

1. `docker-compose up -d --build`

## Running with NodeJS

1. Prepare enviroment file `.env` from an example `.env.example`.
2. Install dependencies: `npm install`.
3. Launch development enviroment: `npm run dev`.