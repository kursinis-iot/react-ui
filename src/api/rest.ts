const API_URL = import.meta.env.VITE_USER_DEVICES_URL;

export type OrderByDirectionType = "asc" | "desc";

export async function request(
    route: string,
    method: string,
    token: string,
    payload: any = undefined
) {
    const response = await fetch(`${API_URL}/${route}`, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': "Bearer " + token
        },
        body: payload === undefined ? undefined : JSON.stringify(payload)
    });

    const {status} = response;
    if (status === 204)
        return null;

    const json = await response.json();
    if (status === 200)
        return json;

    throw json;
}