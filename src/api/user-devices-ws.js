const WS_URL = import.meta.env.VITE_USER_DEVICES_WS_URL;

let currentToken = null;
let wsConnection = null;

export function connect(token, connected = undefined) {
    if (isConnected()) {
        if (currentToken !== token) {
            updateToken(token);
            currentToken = token;
        }

        if (connected !== undefined)
            connected();

        return;
    }

    const webSocket = new WebSocket(`${WS_URL}?token=${token}`);
    webSocket.addEventListener("open", () => onConnected(webSocket, connected));
    webSocket.addEventListener('message', handleMessage);
    webSocket.addEventListener('close', onDisconnect);
    currentToken = token;
}

const callbacks = {};

export function subscribe(
    deviceUuid,
    dataCallback,
    anomalyCallback,
    disconnectedCallback
) {
    callbacks[deviceUuid] = {
        data: dataCallback,
        anomaly: anomalyCallback,
        deviceDisconnected: disconnectedCallback
    };
    wsConnection.send(JSON.stringify({
        type: "subscribe",
        deviceId: deviceUuid
    }));
    wsConnection.send(JSON.stringify({
        type: "subscribeAnomalies",
        deviceId: deviceUuid
    }));
}

export function unsubscribe(deviceUuid) {
    delete callbacks[deviceUuid];
    wsConnection.send(JSON.stringify({
        type: "unsubscribe",
        deviceId: deviceUuid
    }));
    wsConnection.send(JSON.stringify({
        type: "unsubscribeAnomalies",
        deviceId: deviceUuid
    }));
}

function updateToken(token) {
    wsConnection.send(JSON.stringify({
        type: "updateToken",
        token: token
    }));
}

function onConnected(webSocket, connected) {
    console.log("Connected to user-devices-ws!");
    wsConnection = webSocket;

    if (connected !== undefined)
        connected();
}

function handleMessage({data}) {
    const {type, deviceUuid, ...other} = JSON.parse(data);
    const callbackObject = callbacks[deviceUuid];

    if (callbackObject === undefined) {
        console.log(`WS sent data for device: ${deviceUuid}, but no callback was found!`);
        return;
    }

    switch (type) {
        case 'data':
            handleDataMessage(other, callbackObject.data);
            break;
        case 'anomaly':
            handleAnomalyMessage(other, callbackObject.anomaly);
            break;
        case 'deviceDisconnected':
        case 'deviceAnomaliesDisconnected':
            handleDisconnectedMessage(other, callbackObject.deviceDisconnected);
            break;
        default:
            console.log(`user-devices-ws replied with unknown type of message: ${type}`);
            break;
    }
}

function handleDataMessage({payload, timestampMs}, cb) {
    payload['timestamp'] = timestampMs;
    cb(payload);
}

function handleAnomalyMessage({timestampMs}, cb) {
    cb(timestampMs);
}

function handleDisconnectedMessage({reason}, cb) {
    cb(reason);
}

function onDisconnect({reason, code}) {
    if (wsConnection === null)
        return;

    console.log(`user-devices-ws closed connection remote code: ${code}, reason: ${reason}!`);
    wsConnection.close();
    wsConnection = null;
    currentToken = null;
}

export function isConnected() {
    return wsConnection !== null;
}
