import {request} from "./rest.ts";

function mapAlertPayload(alerts) {
    return alerts.map(alert => ({
        ...alert,
        payload: JSON.stringify(alert.payload)
    }));
}

export async function createUserDevice(token, {sensors, key = undefined, ...data}) {
    const payload = {
        ...data,
        sensors: sensors.map(sensor => ({
            ...sensor,
            alerts: mapAlertPayload(sensor.alerts)
        })),
    };

    if (key !== undefined)
        payload.key = key;

    return await request(`devices`, "POST", token, payload);
}

export async function configureUserDevice(token, {uuid, ...payload}) {
    return await request(`devices/${uuid}/configure`, 'PUT', token, payload);
}

export async function configureUserDeviceSensorAlerts(token, {deviceUuid, sensorUuid, alerts}) {
    return await request(`/devices/${deviceUuid}/sensor/${sensorUuid}/alerts`, 'PUT', token, {alerts: mapAlertPayload(alerts)});
}

export async function deleteDevice(token, uuid) {
    return await request(`devices?uuid=${uuid}`, "DELETE", token);
}

export async function fetchUserDeviceSensorHistory(token, deviceUuid, sensorUuid, from, till, granularity) {
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    return await request(`devices/${deviceUuid}/sensor/${sensorUuid}/log?from=${from}&till=${till}&timezone=${timezone}&granularity=${granularity}`, "GET", token);
}