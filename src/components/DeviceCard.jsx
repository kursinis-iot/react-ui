import {
    Avatar,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Flex,
    Heading,
    ListItem,
    UnorderedList
} from "@chakra-ui/react";
import {SensorsTabs} from "./SensorsTabs.jsx";
import {DATETIME_FORMAT} from "../util.jsx";
import moment from "moment";

export function DeviceCard({device, actions}) {
    const lastDataReceived = device.last_sensor_updated_at === null ? "Never" : moment.utc(device.last_sensor_updated_at).local().format(DATETIME_FORMAT);

    return <Card>
        <CardHeader>
            <Flex spacing='4' alignItems='center'>
                <Flex flex='1' gap='4' alignItems='center' flexWrap='wrap'>
                    <Avatar name={device.title}/>
                    <Heading size="sm">{device.title}</Heading>
                </Flex>
            </Flex>
        </CardHeader>
        <CardBody>
            <UnorderedList textAlign={"left"}>
                <ListItem>Key: {device.uuid}</ListItem>
                <ListItem>Status: {device.status}</ListItem>
                <ListItem>Toggle offline after: {device.toggle_offline_after_seconds}s</ListItem>
                <ListItem>Last data received: {lastDataReceived}</ListItem>
                <ListItem>Anomaly Model: {device.anomaly_model?.name ?? "Disabled"}</ListItem>
            </UnorderedList>
        </CardBody>

        <SensorsTabs sensors={device.sensors} tableProps={{maxH: "25vh", overflowY: "auto"}}/>

        <CardFooter
            justify='space-between'
            flexWrap='wrap'
            gap={5}
        >
            {actions}
        </CardFooter>
    </Card>;
}