import {FormControl, FormErrorMessage, FormLabel, Input} from "@chakra-ui/react";

export function InputGroup(
    {
        id,
        label,
        type = "text",
        placeholder = '',
        value,
        onChange,
        errors = [],
        maxLength = undefined,
        minLength = undefined,
        autoComplete = undefined,
        ...props
    }
) {
    return <FormControl py={4} {...props} isInvalid={errors.length !== 0}>
        <FormLabel htmlFor={id}>
            {label}
        </FormLabel>
        <Input
            id={id}
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={(e) => onChange(e.target.value)}
            maxLength={maxLength}
            minLength={minLength}
            autoComplete={autoComplete}
        />
        {errors.map((error, index) => <FormErrorMessage key={index}>{error}</FormErrorMessage>)}
    </FormControl>
}