import {Button, ButtonGroup} from "@chakra-ui/react";
import {Fragment} from "react";

export function Pagination(
    {
        page = 1,
        maxPage = 10,
        onChange = undefined,
        pagesEndVisibleOffset = 3,
        pagesAroundVisibleOffset = 2,
        ...props
    }
) {
    function renderPaginationButton(rpage, index, prevPage = null) {
        let button = <Button
            isDisabled={page === rpage}
            size={"sm"}
            key={index}
            onClick={() => {
                if (onChange !== undefined && onChange !== null)
                    onChange(rpage);
            }}
            disabled={rpage === page}
        >
            {rpage}
        </Button>

        if(prevPage === null || Math.abs(rpage - prevPage) <= 1)
            return button;

        return <Fragment key={index}><span className={"spacer"}>...</span>{button}</Fragment>;
    }

    //Build which pages to render.
    const pagesToRender = [];
    for (let i = 1; i <= pagesEndVisibleOffset && i <= maxPage; i++)
        if (!pagesToRender.includes(i))
            pagesToRender.push(i);

    for (let i = Math.max(1, page - pagesAroundVisibleOffset); i <= page + pagesAroundVisibleOffset && i <= maxPage; i++)
        if (!pagesToRender.includes(i))
            pagesToRender.push(i);

    for (let i = maxPage - pagesEndVisibleOffset + 1; i <= maxPage; i++)
        if (!pagesToRender.includes(i) && i >= 1)
            pagesToRender.push(i);

    return <ButtonGroup className={"pagination"} {...props}>
        {pagesToRender.map((page, index) => renderPaginationButton(
            page,
            index,
            index >= 1 ? pagesToRender[index-1] : null,

        ))}
    </ButtonGroup>

}
