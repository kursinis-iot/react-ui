import {FormControl, FormErrorMessage, FormLabel, Select} from "@chakra-ui/react";

export function SelectGroup({id, label, placeholder = '', value, onChange, errors = [], children, ...props})
{
    return <FormControl py={4} {...props} isInvalid={errors.length !== 0}>
        <FormLabel htmlFor={id}>
            {label}
        </FormLabel>
        <Select
            id={id}
            placeholder={placeholder}
            value={value}
            onChange={(e) => onChange(e.target.value)}
        >
            {children}
        </Select>
        {errors.map((error, index) => <FormErrorMessage key={index}>{error}</FormErrorMessage>)}
    </FormControl>
}