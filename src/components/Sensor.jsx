import {Box, Select, Table, Tbody, Td, Text, Th, Thead, Tr} from "@chakra-ui/react";
import moment from "moment";
import {DATETIME_FORMAT} from "../util.jsx";
import {useState} from "react";
import {Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";

function SensorTable({logs, top = 0}) {
    return <Table>
        <Thead position="sticky" top={top} pt={2} background="white">
            <Tr>
                <Th>Date</Th>
                <Th>Sensor data</Th>
                <Th>Is anomaly</Th>
            </Tr>
        </Thead>
        <Tbody>
            {
                logs.map((log, index) =>
                    <Tr key={index}>
                        <Td>{moment.utc(log.created_at).local().format(DATETIME_FORMAT)}</Td>
                        <Td>{JSON.stringify(log.data)}</Td>
                        <Td>{log.anomaly ? "Yes" : "No"}</Td>
                    </Tr>
                )
            }
        </Tbody>
    </Table>;
}

function SensorGraphDot({cx, cy, r, stroke, payload, value}) {
    const color = payload.anomaly ? '#f00' : stroke;

    return <circle cx={cx} cy={cy} r={r} stroke={color} strokeWidth={1} fill={color}/>
}

function SensorGraph({logs, height}) {
    const data = logs.map(log => ({
        time: moment.utc(log.created_at).local().format(DATETIME_FORMAT),
        sensor: log.data,
        anomaly: log.anomaly ?? false
    }));

    return <Box px={6}>
        <ResponsiveContainer width="100%" height={height}>
            <LineChart data={data}>
                <Line
                    isAnimationActive={false}
                    type={"monotone"}
                    dataKey="sensor"
                    dot={<SensorGraphDot/>}
                    activeDot={false}
                />
                <XAxis dataKey="time" type="category"/>
                <YAxis width={40}/>
                <Tooltip/>
                <Legend/>
            </LineChart>
        </ResponsiveContainer>
    </Box>
}

export function SensorSelect({label, value, onChange, children, top = 0}) {
    return <Box display={"flex"} justifyContent="space-between" px={6} py={2} position={"sticky"} top={top} left={0}
                background="white">
        <Text>{label}</Text>
        <Select maxW="200px" value={value} onChange={e => onChange(e.target.value)} size={"sm"}>
            {children}
        </Select>
    </Box>;
}

export function DefaultSensorSelector({view, setView}) {
    return <SensorSelect label="Display as:" value={view} onChange={setView}>
        <option value={"table"}>Table</option>
        <option value={"graph"}>Graph</option>
    </SensorSelect>
}

export function renderDefaultSensorSelector(view, setView) {
    return <DefaultSensorSelector view={view} setView={setView}/>;
}


export function Sensor({
                           logs,
                           graphHeight = 280,
                           renderSensorFilterComponent = renderDefaultSensorSelector,
                           tableHeadingTop = "48px",
                           ...props
                       }) {
    const [view, setView] = useState("graph");

    let child;
    switch (view) {
        case 'table':
            child = <SensorTable logs={logs} top={tableHeadingTop}/>;
            break;
        case 'graph':
            child = <SensorGraph logs={logs} height={graphHeight}/>;
            break;
    }

    return <Box m={-4} {...props}>
        {renderSensorFilterComponent(view, setView)}
        {child}
    </Box>
}