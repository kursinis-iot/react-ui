import {Tab, TabList, TabPanel, TabPanels, Tabs} from "@chakra-ui/react";
import {Sensor} from "./Sensor.jsx";

export function SensorsTabs({sensors, tableProps = {}, ...props}) {
    return <Tabs isFitted {...props}>
        <TabList>
            {sensors.map((sensor, index) =>
                <Tab key={index}>{sensor.title}</Tab>
            )}
        </TabList>
        <TabPanels>
            {sensors.map((sensor, index) => <TabPanel key={index}>
                <Sensor logs={sensor.minute_of_logs} {...tableProps}/>
            </TabPanel>)}
        </TabPanels>
    </Tabs>
}