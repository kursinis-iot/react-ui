import {useContext, useState} from "react";
import {Box, Fade, IconButton, Link, StackDivider, VStack} from "@chakra-ui/react";
import {ArrowRightIcon} from "@chakra-ui/icons";
import {useLogout} from "@kudze/auth-guard";

export function Sidebar() {
    const [expanded, setExpanded] = useState(true);
    const [transitionDelay, setTransitionDelay] = useState(0);

    const logout = useLogout();

    function minimize() {
        setTransitionDelay(0.25);
        setExpanded(false);
    }

    function expand() {
        setExpanded(true);
    }

    const navLinkProps = {fontSize: "22px", fontWeight: "300"};
    return <Box
        display={"flex"}
        flexDirection={"column"}
        justifyContent={"center"}
        minHeight="100vh"
        backgroundColor={"blackAlpha.50"}
        px={expanded ? 10 : 5}
        py={5}
        width={expanded ? "300px" : "80px"}
        transition={"width 250ms ease-in"}
        whiteSpace={"nowrap"}
    >
        {expanded ? <Fade in={true} delay={transitionDelay}>
                <VStack
                    divider={<StackDivider borderColor='gray.200'/>}
                    spacing={4}
                    align='center'
                >
                    <Link {...navLinkProps} href={"/"}>Home</Link>
                    <Link {...navLinkProps} href={"/devices"}>My Devices</Link>
                    <Link {...navLinkProps} onClick={logout}>Logout</Link>
                    <Link {...navLinkProps} onClick={minimize}>Minimize</Link>
                </VStack>
            </Fade> :
            <IconButton aria-label={"expand"} onClick={expand}>
                <ArrowRightIcon/>
            </IconButton>
        }

    </Box>

}