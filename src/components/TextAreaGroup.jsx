import {FormControl, FormErrorMessage, FormLabel, Textarea} from "@chakra-ui/react";

export function TextAreaGroup({id, label, placeholder = '', value, onChange, errors = [], rows = undefined, ...props}) {
    return <FormControl py={4} {...props} isInvalid={errors.length !== 0}>
        <FormLabel htmlFor={id}>
            {label}
        </FormLabel>
        <Textarea
            id={id}
            rows={rows}
            placeholder={placeholder}
            value={value}
            onChange={(e) => onChange(e.target.value)}
        />
        {errors.map((error, index) => <FormErrorMessage key={index}>{error}</FormErrorMessage>)}
    </FormControl>
}