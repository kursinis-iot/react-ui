import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Button, Input, Tab, TabList, TabPanel, TabPanels, Tabs,
    Text
} from "@chakra-ui/react";
import {InputGroup} from "./InputGroup.jsx";
import {SelectGroup} from "./SelectGroup.jsx";

export const ALERT_HANDLERS = {
    range: DeviceSensorAlertRangeConfig,
    downtime: DeviceSensorDowntimeConfig
};

const ALERT_LABELS = {
    range: "Range (Sensor value must be in the range)",
    downtime: "Downtime (Last sensor value received not longer than)",
}

export const ALERT_TYPES = Object.keys(ALERT_HANDLERS);

function getDefaultAlertTypeConfig(type) {
    switch (type) {
        case "range":
            return {
                min: 0,
                max: 100
            };
        case "downtime":
            return 3600;
        default:
            return undefined;
    }
}

export function DeviceSensorAlertsAccordion({alerts, errors, setAlert, deleteAlert, errorPrefix = 'alerts'}) {
    return <Accordion allowMultiple>
        {alerts.map((alert, index) => <DeviceSensorAlertAccordionItem
            key={index}
            index={index}
            alert={alert}
            errors={errors}
            errorPrefix={errorPrefix}
            deleteAlert={deleteAlert.bind(this, index)}
            setAlert={setAlert.bind(this, index)}
        />)}
    </Accordion>;
}

export function DeviceSensorAlertAccordionItem(
    {
        alert,
        index,
        errors,
        errorPrefix,
        deleteAlert,
        setAlert
    }
) {
    const AlertConfig = ALERT_HANDLERS[alert.type];

    function addEmailRecipient() {
        setAlert({
            ...alert,
            email_recipients: [...alert.email_recipients, '']
        });
    }

    function setEmailRecipient(index, email) {
        const emailRecipients = [...alert.email_recipients];
        emailRecipients[index] = email;

        setAlert({
            ...alert,
            email_recipients: emailRecipients
        });
    }

    function deleteEmailRecipient(index) {
        const emailRecipients = [...alert.email_recipients];
        emailRecipients.splice(index, 1);

        setAlert({
            ...alert,
            email_recipients: emailRecipients
        });
    }

    const anyAlertErrors = errors !== undefined && Object.keys(errors).some(error => error.startsWith(`${errorPrefix}.${index}`));

    const anyTitleErrors = errors !== undefined && Object.keys(errors).some(error => error === `${errorPrefix}.${index}.title`);
    const anyIntervalErrors = errors !== undefined && Object.keys(errors).some(error => error === `${errorPrefix}.${index}.interval`);
    const anyTypeErrors = errors !== undefined && Object.keys(errors).some(error => error === `${errorPrefix}.${index}.type`);
    const anyPayloadErrors = errors !== undefined && Object.keys(errors).some(error => error === `${errorPrefix}.${index}.payload`);

    const anyDataErrors = anyTitleErrors || anyIntervalErrors || anyTitleErrors || anyPayloadErrors;
    const anyEmailErrors = errors !== undefined && Object.keys(errors).some(error => error.startsWith(`${errorPrefix}.${index}.email_recipients`));

    return <AccordionItem>
        <AccordionButton>
            <Box
                flex='1'
                display={"flex"}
                alignItems={"center"}
                color={anyAlertErrors ? "red" : null}
            >
                Alert #{index} {alert.title}
                <Button
                    colorScheme={"red"}
                    ml={1.5}
                    size={"xs"}
                    aria-label={"delete alert"}
                    onClick={deleteAlert}
                >-</Button>
            </Box>
            <AccordionIcon/>
        </AccordionButton>

        <AccordionPanel p={0}>
            <Tabs isFitted>
                <TabList>
                    <Tab
                        color={anyDataErrors ? "red" : null}
                    >Data</Tab>
                    <Tab
                        color={anyEmailErrors ? "red" : null}
                    >
                        Email Recipients ({alert.email_recipients.length})

                        <Button
                            colorScheme={"green"}
                            ml={1.5}
                            size={"xs"}
                            aria-label={"add recipient"}
                            onClick={addEmailRecipient}
                        >+</Button>
                    </Tab>
                </TabList>

                <TabPanels>
                    <TabPanel>
                        <InputGroup
                            id={`add_device_${index}_sensor_alert_name`}
                            type={"text"}
                            label={"Alert title:"}
                            value={alert.title}
                            onChange={(title) => setAlert({...alert, title: title})}
                            errors={anyTitleErrors ? errors[`${errorPrefix}.${index}.title`] : undefined}
                        />

                        <InputGroup
                            id={`add_device_${index}_sensor_alert_interval`}
                            type={"number"}
                            label={"Alert interval (seconds):"}
                            value={alert.interval}
                            onChange={(interval) => setAlert({...alert, interval: interval})}
                            errors={anyIntervalErrors ? errors[`${errorPrefix}.${index}.interval`] : undefined}
                        />

                        <SelectGroup
                            id={`add_device_${index}_sensor_alert_type`}
                            label={"Alert type:"}
                            value={alert.type}
                            onChange={(type) => setAlert({
                                ...alert,
                                type: type,
                                payload: getDefaultAlertTypeConfig(type)
                            })}
                            errors={anyTypeErrors ? errors[`${errorPrefix}.${index}.type`] : undefined}
                        >
                            {ALERT_TYPES.map((type, index) =>
                                <option
                                    key={index}
                                    value={type}
                                >
                                    {ALERT_LABELS[type]}
                                </option>
                            )}
                        </SelectGroup>

                        <Text fontWeight="bold">Alert payload config:</Text>
                        {<AlertConfig alert={alert} setAlert={setAlert} index={index}/>}
                    </TabPanel>

                    <TabPanel>

                        {alert.email_recipients.map((email, _index) => <DeviceSensorAlertEmailRecipientGroup
                            alertIndex={index}
                            index={_index}
                            key={_index}
                            errorPrefix={errorPrefix}
                            errors={errors}
                            setEmailRecipient={setEmailRecipient.bind(this, _index)}
                            deleteEmailRecipient={deleteEmailRecipient.bind(this, _index)}
                            emailRecipient={email}
                        />)}

                    </TabPanel>
                </TabPanels>

            </Tabs>
        </AccordionPanel>
    </AccordionItem>
}

export function DeviceSensorAlertEmailRecipientGroup(
    {
        errorPrefix,
        errors,
        alertIndex,
        index,
        setEmailRecipient,
        deleteEmailRecipient,
        emailRecipient
    }
) {
    const hasErrors = errors !== undefined && Object.keys(errors).some(error => error === `${errorPrefix}.${alertIndex}.email_recipients.${index}`);

    return <Box display={"flex"} alignItems={"flex-end"}>
        <InputGroup
            id={`add_device_${alertIndex}_sensor_alert_${index}_recipient_email`}
            label={`Email ${index + 1}:`}
            value={emailRecipient.email}
            onChange={(email) => setEmailRecipient({...emailRecipient, email: email})}
            errors={hasErrors ? errors[`${errorPrefix}.${alertIndex}.email_recipients.${index}.email`] : undefined}
        />
        <InputGroup
            id={`add_device_${alertIndex}_sensor_alert_${index}_recipient_first_name`}
            ml={1.5}
            label={`First name ${index + 1}:`}
            value={emailRecipient.first_name}
            onChange={(first_name) => setEmailRecipient({...emailRecipient, first_name: first_name})}
            errors={hasErrors ? errors[`${errorPrefix}.${alertIndex}.email_recipients.${index}.first_name`] : undefined}
        />
        <InputGroup
            id={`add_device_${alertIndex}_sensor_alert_${index}_recipient_last_name`}
            ml={1.5}
            label={`Last name ${index + 1}:`}
            value={emailRecipient.last_name}
            onChange={(last_name) => setEmailRecipient({...emailRecipient, last_name: last_name})}
            errors={hasErrors ? errors[`${errorPrefix}.${alertIndex}.email_recipients.${index}.last_name`] : undefined}
        />
        <SelectGroup
            id={`add_device_${alertIndex}_sensor_alert_${index}_recipient_language`}
            ml={1.5}
            label={`Language ${index + 1}:`}
            value={emailRecipient.language}
            onChange={(language) => setEmailRecipient({...emailRecipient, language: language})}
            errors={hasErrors ? errors[`${errorPrefix}.${alertIndex}.email_recipients.${index}.language`] : undefined}
        >
            <option value="lt">Lithuanian</option>
            <option value="en">English</option>
        </SelectGroup>
        <Button
            marginY={"16px"}
            marginBottom={hasErrors ? "40px" : undefined}
            colorScheme={"red"}
            ml={1.5}
            size={"md"}
            aria-label={"delete recipient"}
            onClick={deleteEmailRecipient}
        >-</Button>
    </Box>;
}

export function DeviceSensorAlertRangeConfig({alert, setAlert, index}) {
    return <>
        <InputGroup
            id={`add_device_${index}_sensor_alert_payload_min`}
            type={'number'}
            label={"Min bound:"}
            value={alert.payload.min}
            onChange={(value) => setAlert({...alert, payload: {...alert.payload, min: value}})}
        />

        <InputGroup
            id={`add_device_${index}_sensor_alert_payload_max`}
            type={'number'}
            label={"Max bound:"}
            value={alert.payload.max}
            onChange={(value) => setAlert({...alert, payload: {...alert.payload, max: value}})}
        />
    </>;
}

export function DeviceSensorDowntimeConfig({alert, setAlert, index}) {
    return <InputGroup
        id={`add_device_${index}_sensor_alert_payload`}
        type={'number'}
        label={"Downtime interval (seconds):"}
        value={alert.payload}
        onChange={(value) => setAlert({...alert, payload: value})}
    />;
}