import {Accordion} from "@chakra-ui/react";
import {SensorAccordionItem} from "@/components/forms/AddDeviceForm/DeviceSensorTab/sensorAccordionItem.jsx";

export function DeviceSensorTab({config, errors, setConfig}) {
    function setSensor(index, sensor) {
        const configUpdated = {...config, sensors: [...config.sensors]};
        configUpdated.sensors[index] = {...sensor};
        setConfig(configUpdated);
    }

    function removeSensor(index) {
        const sensorsUpdated = [...config.sensors];
        sensorsUpdated.splice(index, 1)
        setConfig({...config, sensors: sensorsUpdated});
    }

    return <Accordion allowMultiple>
        {config.sensors.map((sensor, index) => <SensorAccordionItem
            key={index}
            index={index}
            sensor={sensor}
            setSensor={setSensor.bind(this, index)}
            removeSensor={removeSensor.bind(this, index)}
            errors={errors}
        />)}
    </Accordion>
}