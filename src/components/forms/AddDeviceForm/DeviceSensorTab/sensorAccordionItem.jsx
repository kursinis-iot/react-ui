import {
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Button, Tab,
    TabList, TabPanel, TabPanels,
    Tabs
} from "@chakra-ui/react";
import {InputGroup} from "../../../InputGroup.jsx";
import {DeviceSensorAlertsAccordion} from "../../../deviceSensorAccordion.jsx";
import {DEFAULT_ALERT} from "../index.jsx";

export function SensorAccordionItem({index, sensor, errors, setSensor, removeSensor}) {
    function addAlert() {
        setSensor({
            ...sensor,
            alerts: [...sensor.alerts, {...DEFAULT_ALERT}]
        })
    }

    function setAlert(alertIndex, alert) {
        const alerts = [...sensor.alerts];
        alerts[alertIndex] = alert;

        setSensor({
            ...sensor,
            alerts: alerts
        });
    }

    function deleteAlert(alertIndex) {
        const alerts = [...sensor.alerts];
        alerts.splice(alertIndex, 1);

        setSensor({
            ...sensor,
            alerts: alerts
        });
    }

    const anySensorErrors = errors !== undefined && Object.keys(errors).some(error => error.startsWith(`sensors.${index}.`));
    const anyDataErrors = errors !== undefined && Object.keys(errors).some(error => error === `sensors.${index}.title` || error === `sensors.${index}.query`);
    const anyAlertErrors = errors !== undefined && Object.keys(errors).some(error => error.startsWith(`sensors.${index}.alerts`));

    return <AccordionItem>
        <AccordionButton>
            <Box
                flex='1'
                display={"flex"}
                alignItems={"center"}
                color={anySensorErrors ? "red" : null}
            >
                Sensor #{index}: {sensor.title}
                <Button
                    colorScheme={"red"}
                    ml={1.5}
                    size={"xs"}
                    aria-label={"remove sensor"}
                    onClick={removeSensor}
                >-</Button>
            </Box>
            <AccordionIcon/>
        </AccordionButton>

        <AccordionPanel px={0}>
            <Tabs isFitted>
                <TabList>
                    <Tab color={anyDataErrors ? "red" : null}>Data</Tab>
                    <Tab color={anyAlertErrors ? "red" : null}>
                        Alerts ({sensor.alerts.length})

                        <Button
                            colorScheme={"green"}
                            ml={1.5}
                            size={"xs"}
                            aria-label={"add alert"}
                            onClick={addAlert}
                        >+</Button>
                    </Tab>
                </TabList>
                <TabPanels>
                    <TabPanel>
                        <InputGroup
                            id={`add_device_sensor_${index}_title`}
                            label={"Sensor title:"}
                            errors={errors !== undefined && Object.keys(errors).some(error => error === `sensors.${index}.title`) ? errors[`sensors.${index}.title`] : undefined}
                            value={sensor.title}
                            onChange={(value) => setSensor({...sensor, title: value})}
                        />
                        <InputGroup
                            id={`add_device_sensor_${index}_query`}
                            label={"Data JSONPath query:"}
                            errors={errors !== undefined && Object.keys(errors).some(error => error === `sensors.${index}.query`) ? errors[`sensors.${index}.query`] : undefined}
                            value={sensor.query}
                            onChange={(value) => setSensor({...sensor, query: value})}
                        />
                    </TabPanel>

                    <TabPanel p={0}>
                        <DeviceSensorAlertsAccordion
                            alerts={sensor.alerts}
                            errors={errors}
                            setAlert={setAlert}
                            deleteAlert={deleteAlert}
                            errorPrefix={`sensors.${index}.alerts`}
                        />
                    </TabPanel>
                </TabPanels>
            </Tabs>
        </AccordionPanel>
    </AccordionItem>;
}