import {useEffect, useState} from "react";
import {fetchAnomalyModels} from "@/repositories/anomalyModels.ts";
import {useAccessToken} from "@kudze/auth-guard";
import {SelectGroup} from "@/components/SelectGroup.jsx";
import {Spinner} from "@chakra-ui/react";

export function DeviceAnomalyDetectionField(
    {
        id,
        errors,
        value,
        onChange
    }
) {
    const token = useAccessToken();
    const [models, setModels] = useState(undefined);

    useEffect(() => {
        fetchAnomalyModels(token).then(setModels);
    }, []);

    if (models === undefined)
        return <Spinner/>;

    return <>
        <SelectGroup
            id={id}
            label={"Anomaly detection model:"}
            value={value ?? ''}
            onChange={(val) => onChange(val.length === 0 ? null : val)}
            errors={errors}
        >
            <option value={""}>Disabled</option>
            {models.map((model, index) =>
                <option
                    key={index}
                    value={model.uuid}
                >
                    {model.name}
                </option>
            )}
        </SelectGroup>
    </>;
}