import {DeviceAnomalyDetectionField} from "@/components/forms/AddDeviceForm/deviceAnomalyDetectionField.jsx";

export function DeviceAnomalyDetectionTab({config, setConfig, errors}) {
    return <DeviceAnomalyDetectionField
        id={'add_device_anomaly_model'}
        value={config.anomaly_model_uuid}
        onChange={(anomalyModelUuid) => setConfig({
            ...config,
            anomaly_model_uuid: anomalyModelUuid,
        })}
        errors={errors ? errors[`anomaly_model_uuid`] : undefined}
    />;
}