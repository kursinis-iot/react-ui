import {TextAreaGroup} from "../../TextAreaGroup.jsx";

export function DeviceConfigTab({config, errors, setConfig}) {
    return <TextAreaGroup
        id="add_device_config"
        label="Configuration:"
        value={config.config}
        errors={errors?.config}
        onChange={(_config) => setConfig({...config, config: _config})}
    />
}