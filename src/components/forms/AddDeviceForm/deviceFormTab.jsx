import {Checkbox, TabPanel} from "@chakra-ui/react";
import {InputGroup} from "../../InputGroup.jsx";

export function DeviceFormTab({config, errors, setConfig}) {
    function onKeyCheckbox() {
        setConfig({...config, key: config.key === undefined ? "" : undefined})
    }

    return <TabPanel p={0}>
        <InputGroup
            id={"add_device_title"}
            label={"Device title:"}
            value={config.title}
            errors={errors?.title}
            onChange={(title) => setConfig({...config, title: title})}
        />

        <Checkbox colorScheme='red' isChecked={config.key !== undefined} onChange={onKeyCheckbox}>
            Specify device key
        </Checkbox>

        {config.key !== undefined ? <InputGroup
            id={'add_device_key'}
            label={"Device key:"}
            value={config.key}
            errors={errors?.key}
            onChange={(key) => setConfig({...config, key: key})}
        /> : null}

        <InputGroup
            id={"add_device_offline_time"}
            type={"number"}
            label={"Offline time:"}
            value={config.offline_time}
            errors={errors?.offline_time}
            onChange={(title) => setConfig({...config, offline_time: title})}
        />
    </TabPanel>;
}