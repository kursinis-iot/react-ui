import {
    Box,
    Button,
    Tab,
    TabList,
    TabPanel,
    TabPanels,
    Tabs
} from "@chakra-ui/react";
import {createUserDevice} from "@/api/user-devices.js";
import {useAccessToken} from "@kudze/auth-guard";
import {useState} from "react";
import {DeviceSensorTab} from "@/components/forms/AddDeviceForm/DeviceSensorTab";
import {DeviceFormTab} from "@/components/forms/AddDeviceForm/deviceFormTab.jsx";
import {DeviceConfigTab} from "@/components/forms/AddDeviceForm/deviceConfigTab.jsx";
import {DeviceAnomalyDetectionTab} from "@/components/forms/AddDeviceForm/deviceAnomalyDetectionTab.jsx";

export function AddDeviceForm({onDeviceAdded}) {
    const accessToken = useAccessToken();

    const [config, setConfig] = useState({
        title: '',
        key: undefined,
        offline_time: 5,
        sensors: [],
        config: '{}',
        anomaly_model_uuid: null
    });
    const [errors, setErrors] = useState(undefined);

    function addSensor() {
        setConfig({
            ...config, sensors: [...config.sensors, {
                title: '',
                query: '',
                alerts: []
            }]
        });
    }

    function onContinue() {
        createUserDevice(accessToken, config)
            .then(onDeviceAdded)
            .catch(errors => setErrors(errors));
    }

    function onSubmit(e) {
        e.preventDefault();
        onContinue();
    }

    return <form onSubmit={onSubmit}>
        <Tabs isFitted>
            <TabList>
                <Tab color={errors?.key !== undefined || errors?.title !== undefined ? "red" : null}>Device</Tab>
                <Tab color={errors?.config !== undefined ? "red" : null}>Configuration</Tab>
                <Tab
                    color={errors !== undefined && Object.keys(errors).some((error) => error.startsWith("sensors")) ? "red" : null}>
                    Sensors ({config.sensors.length})
                    <Button
                        colorScheme={"green"}
                        ml={1.5}
                        size={"xs"}
                        aria-label={"add sensor"}
                        onClick={addSensor}
                    >+</Button>
                </Tab>
                <Tab>Anomaly Detection</Tab>
            </TabList>

            <TabPanels>
                <TabPanel p={0}>
                    <DeviceFormTab errors={errors} config={config} setConfig={setConfig}/>
                </TabPanel>
                <TabPanel>
                    <DeviceConfigTab errors={errors} config={config} setConfig={setConfig}/>
                </TabPanel>
                <TabPanel p={0}>
                    <DeviceSensorTab errors={errors} config={config} setConfig={setConfig}/>
                </TabPanel>
                <TabPanel>
                    <DeviceAnomalyDetectionTab errors={errors} config={config} setConfig={setConfig}/>
                </TabPanel>
            </TabPanels>
        </Tabs>

        <Box py={5} textAlign={"center"}>
            <Button type='submit' colorScheme='green' mx={"auto"}>Create</Button>
        </Box>
    </form>
}

export const DEFAULT_ALERT = {
    title: "",
    type: "range",
    interval: 3600,
    email_recipients: [],
    payload: {
        min: 0,
        max: 100
    }
};