import {DeviceSensorAlertsAccordion} from "../deviceSensorAccordion.jsx";
import {useState} from "react";
import {SelectGroup} from "../SelectGroup.jsx";
import {Button} from "@chakra-ui/react";
import {DEFAULT_ALERT} from "./AddDeviceForm/index.jsx";
import {configureUserDeviceSensorAlerts} from "../../api/user-devices.js";
import {useAccessToken} from "@kudze/auth-guard";

export function ConfigureDeviceAlerts({device, onAlertsConfigured}) {
    const [sensorIndex, setSensorIndex] = useState(0);
    const [alerts, setAlerts] = useState(buildAlertsFromIndex(sensorIndex));
    const [errors, setErrors] = useState(undefined);
    const accessToken = useAccessToken();

    function setAlert(index, alert) {
        const _alerts = [...alerts];
        _alerts[index] = alert;
        setAlerts(_alerts);
    }

    function deleteAlert(index) {
        const _alerts = [...alerts];
        _alerts.splice(index, 1);
        setAlerts(_alerts);
    }

    function addAlert() {
        setAlerts([...alerts, {...DEFAULT_ALERT}]);
    }

    function buildAlertsFromIndex(index) {
        return device.sensors[index].alerts.map(alert => ({
            ...alert,
            email_recipients: alert.email_recipients.map((email) => ({
                email: email.email,
                first_name: email.first_name,
                last_name: email.last_name,
                language: email.language
            }))
        }));
    }

    function onSwapSensor(sensorIndex) {
        setAlerts(buildAlertsFromIndex(sensorIndex));
        setSensorIndex(sensorIndex);
    }

    function onSubmit(e) {
        e.preventDefault();

        configureUserDeviceSensorAlerts(accessToken, {
            deviceUuid: device.uuid,
            sensorUuid: device.sensors[sensorIndex].uuid,
            alerts: alerts
        }).then(onAlertsConfigured).catch(e => setErrors(e));
    }

    return <form onSubmit={onSubmit}>
        <SelectGroup
            id={`configure-device-${device.uuid}-alerts-sensor`}
            label={"Choose sensor:"}
            value={sensorIndex}
            onChange={onSwapSensor}
        >
            {device.sensors.map((sensor, index) => <option value={index} key={index}>{sensor.title}</option>)}
        </SelectGroup>

        {alerts === undefined ? null :
            <>
                <DeviceSensorAlertsAccordion
                    alerts={alerts}
                    errors={errors}
                    setAlert={setAlert}
                    deleteAlert={deleteAlert}
                />
                <Button my={5} display="block" mx="auto" colorScheme="green" onClick={addAlert}>Add Alert</Button>
            </>
        }

        <Button mt={5} display="block" mx="auto" colorScheme="green" type="submit">Save</Button>
    </form>
}