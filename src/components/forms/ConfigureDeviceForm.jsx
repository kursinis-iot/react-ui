import {useState} from "react";
import {TextAreaGroup} from "../TextAreaGroup.jsx";
import {Button} from "@chakra-ui/react";
import {configureUserDevice} from "@/api/user-devices.js";
import {useAccessToken} from "@kudze/auth-guard";
import {DeviceAnomalyDetectionField} from "@/components/forms/AddDeviceForm/deviceAnomalyDetectionField.jsx";

export function ConfigureDeviceForm({onDeviceConfigured, device}) {
    const [config, setConfig] = useState(JSON.stringify(device.config, null, 4));
    const [anomalyModelUuid, setAnomalyModelUuid] = useState(device.anomaly_model_uuid);
    const [errors, setErrors] = useState(undefined);
    const accessToken = useAccessToken();

    function submit(e) {
        e.preventDefault();

        configureUserDevice(accessToken, {
            uuid: device.uuid,
            config: config,
            anomaly_model_uuid: anomalyModelUuid
        }).then(onDeviceConfigured).catch(e => setErrors(e));
    }

    return <form onSubmit={submit}>
        <TextAreaGroup
            id={`configure_device_${device.uuid}_config`}
            label="Configuration:"
            value={config}
            errors={errors?.config}
            onChange={(config) => setConfig(config)}
            rows={10}
        />
        <DeviceAnomalyDetectionField
            id={`configure_device_${device.uuid}_anomaly_model`}
            errors={errors?.anomaly_model_uuid}
            value={anomalyModelUuid}
            onChange={setAnomalyModelUuid}
        />
        <Button
            type="submit"
            mx="auto"
            display="block"
            mb={5}
            mt={2}
            colorScheme="green"
        >Configure</Button>
    </form>;
}