import moment from "moment";
import {useState} from "react";
import {Button} from "@chakra-ui/react";

import {SelectGroup} from "../SelectGroup.jsx";
import {fetchUserDeviceSensorHistory} from "../../api/user-devices.js";
import {DATETIME_FORMAT} from "../../util.jsx";
import {renderDefaultSensorSelector, Sensor, SensorSelect} from "../Sensor.jsx";
import {InputGroup} from "../InputGroup.jsx";
import {useAccessToken} from "@kudze/auth-guard";

export function DeviceSensorHistoryFilterForm({device, onDataFetched}) {
    const accessToken = useAccessToken();
    const [data, setData] = useState({
        sensorUuid: device.sensors[0].uuid,
        from: moment().startOf("month").format(DATETIME_FORMAT),
        till: moment().format(DATETIME_FORMAT),
        granularity: 'raw'
    });
    const [errors, setErrors] = useState(undefined);

    function onSubmit(e) {
        e.preventDefault();
        fetchUserDeviceSensorHistory(
            accessToken,
            device.uuid,
            data.sensorUuid,
            data.from,
            data.till,
            data.granularity
        ).then(onDataFetched).catch(e => setErrors(e));
    }

    return <form onSubmit={onSubmit}>
        <SelectGroup
            id={"device-sensor-history-filter-sensor"}
            label={"Choose sensor"}
            value={data.sensorUuid}
            onChange={sensorUuid => setData({...data, sensorUuid: sensorUuid})}
            errors={errors !== undefined ? errors['sensor_uuid'] ?? [] : []}
        >
            {device.sensors.map((sensor, index) => <option key={index} value={sensor.uuid}>{sensor.title}</option>)}
        </SelectGroup>

        <InputGroup
            id={'device-sensor-history-filter-from'}
            label={"From"}
            type={"text"}
            value={data.from}
            onChange={from => setData({...data, from: from})}
            errors={
                errors === undefined ? [] :
                    (errors['timeframe'] ?? []).concat(
                        errors['from'] ?? []
                    )
            }
        />

        <InputGroup
            id={'device-sensor-history-filter-from'}
            label={"Till"}
            type={"text"}
            value={data.till}
            onChange={till => setData({...data, till: till})}
            errors={
                errors === undefined ? [] :
                    (errors['timeframe'] ?? []).concat(
                        errors['till'] ?? []
                    )
            }
        />

        <SelectGroup
            id={"device-sensor-history-filter-granularity"}
            label={"Choose granularity"}
            value={data.granularity}
            onChange={granularity => setData({...data, granularity: granularity})}
            errors={errors !== undefined ? errors['granularity'] ?? [] : []}
        >
            <option value="raw">Raw</option>
            <option value="minute">Each minute</option>
            <option value="hour">Hourly</option>
            <option value="day">Daily</option>
        </SelectGroup>

        <Button type={"submit"} mx="auto" display="block" colorScheme="green" mt={5} mb={2}>Continue</Button>
    </form>
}

const GROUP_BY = {
    raw: "Don't group",
    eachMinute: "Each minute",
    hourly: "Hourly",
    daily: "Daily"
};

function renderSensorFilterComponent(view, setView, groupBy, setGroupBy) {
    return <>
        {renderDefaultSensorSelector(view, setView)}
        <SensorSelect label={"Group by"} value={groupBy} onChange={setGroupBy} top="48px">
            {Object.keys(GROUP_BY).map((value, index) => <option key={index} value={value}>{GROUP_BY[value]}</option>)}
        </SensorSelect>
    </>;
}

//TODO: this will be moved to backend at some stage, rn we just support int and array of ints
function computeGroupedValue(data) {
    if (data.length === 0)
        return 0;

    const firstData = data[0].data;
    switch (typeof firstData) {
        case 'number':
            return computeGroupedValueInteger(data);
        case 'object':
            return computeGroupedValueArray(data);
    }
}

function computeGroupedValueInteger(data) {
    const total = data.length;
    let avg = 0;

    data.forEach((log) => avg += log.data / total);

    return avg;
}

function computeGroupedValueArray(data) {
    const total = data.length;

    const firstData = data[0].data;
    let avg = firstData.map(_ => 0);

    data.forEach(
        (log) => log.data.forEach(
            (entry, index) => {
                avg[index] += entry / total
            }
        )
    );

    return avg;
}

function computeGrouped(data, interval, unit = "seconds") {
    const from = moment(data[0].created_at);
    const till = moment(data[data.length - 1].created_at);

    const result = [];

    let current = from;

    const dataCopy = [...data];

    while (current <= till) {
        const nextCurrent = current.add(interval, unit)
        let items = 0;
        for (let i = 0; i < dataCopy.length; i++) {
            const v = dataCopy[i];

            if (moment(v.created_at) > nextCurrent)
                break;

            items++;
        }

        result.push({
            created_at: current.format(DATETIME_FORMAT),
            data: computeGroupedValue(dataCopy.splice(0, items))
        })
        current = nextCurrent;
    }

    return result;
}

export function DeviceSensorHistoryFilterFormResult({data, graphHeight = 600}) {
    if (data === undefined)
        return null;

    return <Sensor
        logs={data}
        graphHeight={graphHeight}
        tableHeadingTop={"96px"}
    />
}