import {
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay,
} from "@chakra-ui/react";
import {AddDeviceForm} from "../forms/AddDeviceForm";
import {useState} from "react";
import {ShowDeviceApiKeyModal} from "@/components/modals/ShowDeviceApiKeyModal.jsx";


export function AddDeviceModal({onClose, onCreate, isOpen}) {
    const [apiKeyModalState, setApiKeyModalState] = useState(undefined);

    function onCreated(state) {
        onClose();
        setApiKeyModalState(state)
    }

    function onFinish() {
        setApiKeyModalState(undefined);
        onCreate();
    }

    return <>
        <ShowDeviceApiKeyModal
            apiKey={apiKeyModalState?.api_key}
            onClose={onFinish}
            isOpen={apiKeyModalState !== undefined}
            device={apiKeyModalState?.device}
        />
        <Modal isOpen={isOpen} onClose={onClose} size="4xl">
            <ModalOverlay/>
            <ModalContent>
                <ModalHeader>Add device!</ModalHeader>
                <ModalCloseButton/>
                <ModalBody>
                    <AddDeviceForm onDeviceAdded={onCreated}/>
                </ModalBody>
            </ModalContent>
        </Modal>
    </>
}