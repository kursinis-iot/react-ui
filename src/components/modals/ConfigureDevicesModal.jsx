import {
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay, Tab,
    TabList, TabPanel, TabPanels,
    Tabs
} from "@chakra-ui/react";
import {ConfigureDeviceForm} from "../forms/ConfigureDeviceForm.jsx";
import {ConfigureDeviceAlerts} from "../forms/ConfigureDeviceAlerts.jsx";
import {de} from "date-fns/locale";

export function ConfigureDevicesModal({onClose, onSuccess, device}) {
    return <Modal isOpen={device !== undefined} onClose={onClose} size="4xl">
        <ModalOverlay/>
        <ModalContent>
            <ModalHeader>Configure the device!</ModalHeader>
            <ModalCloseButton/>
            <ModalBody>
                <Tabs isFitted>
                    <TabList>
                        <Tab>Configuration</Tab>
                        <Tab>Alerts</Tab>
                    </TabList>

                    <TabPanels>
                        <TabPanel>
                            <ConfigureDeviceForm onDeviceConfigured={onSuccess} device={device}/>
                        </TabPanel>

                        <TabPanel>
                            <ConfigureDeviceAlerts onAlertsConfigured={onSuccess} device={device}/>
                        </TabPanel>
                    </TabPanels>
                </Tabs>
            </ModalBody>
        </ModalContent>
    </Modal>
}