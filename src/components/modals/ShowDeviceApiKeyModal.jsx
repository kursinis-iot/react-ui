import {
    Box,
    Button,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent, ModalFooter,
    ModalHeader,
    ModalOverlay,
    Text,
} from "@chakra-ui/react";

export function ShowDeviceApiKeyModal({onClose, device, apiKey, isOpen}) {
    return <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay/>
        <ModalContent>
            <ModalHeader>{device?.title} API key</ModalHeader>
            <ModalCloseButton/>
            <ModalBody>
                <Text mb={2}>Make sure to save the API key, because it will be shown only once!</Text>

                <Box p={5} backgroundColor="#00000008">
                    <code>{apiKey}</code>
                </Box>
            </ModalBody>
            <ModalFooter textAlign="center">
                <Button onClick={onClose} colorScheme='green' mx="auto">Continue</Button>
            </ModalFooter>
        </ModalContent>
    </Modal>
}