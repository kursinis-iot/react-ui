import {
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay, Tab,
    TabList, TabPanel, TabPanels,
    Tabs
} from "@chakra-ui/react";
import {useState} from "react";
import {
    DeviceSensorHistoryFilterForm,
    DeviceSensorHistoryFilterFormResult
} from "../forms/DeviceSensorHistoryFilterForm.jsx";

export function ViewSensorHistoryModal({device, onClose}) {
    const [data, setData] = useState(undefined);
    const [tabIndex, setTabIndex] = useState(0);

    function onDataFetched(data) {
        setData(data);
        setTabIndex(1);
    }

    return <Modal isOpen={device !== undefined} onClose={onClose} size="full">
        <ModalOverlay/>
        <ModalContent>
            <ModalHeader>{device?.title} history</ModalHeader>
            <ModalCloseButton/>
            <ModalBody>

                <Tabs isFitted onChange={setTabIndex} index={tabIndex}>
                    <TabList>
                        <Tab>Configure</Tab>
                        <Tab isDisabled={data === undefined}>Data</Tab>
                    </TabList>

                    <TabPanels>
                        <TabPanel>
                            <DeviceSensorHistoryFilterForm device={device} onDataFetched={onDataFetched}/>
                        </TabPanel>

                        <TabPanel>
                            <DeviceSensorHistoryFilterFormResult data={data} graphHeight={600}/>
                        </TabPanel>
                    </TabPanels>
                </Tabs>

            </ModalBody>
        </ModalContent>
    </Modal>
}