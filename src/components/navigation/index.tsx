import {useLocation, useNavigate} from "react-router-dom";
import {Button, cn} from "@kudze/auth-guard";

export type NavigationProps = {
    routes: {[key: string]: string}
}

export function Navigation(
    {
        routes
    }: NavigationProps
) {
    const location = useLocation();
    const navigate = useNavigate();

    const isRouteActive = (route: string) => location.pathname == route;

    const buttonClassNames = 'block min-w-max w-100';
    const activeButtonClassNames = cn(buttonClassNames, 'opacity-100');

    return <>
        {Object.keys(routes).map((route, index) => isRouteActive(route) ?
            <Button key={index} className={activeButtonClassNames} variant={"default"} disabled>{routes[route]}</Button> :
            <Button key={index} className={buttonClassNames} variant={"secondary"}
                    onClick={() => navigate(route)}>{routes[route]}</Button>
        )}
    </>
}