import {useTranslation} from "react-i18next";
import {Navigation} from "@/components/navigation/index.tsx";
import {DEVICES_ROUTE, HOME_ROUTE, OG_DEVICES_ROUTE} from "@/router.tsx";

export function RootNavigation() {
    const {t} = useTranslation();
    const routes = {
        [HOME_ROUTE]: t('navigation.home'),
        [DEVICES_ROUTE]: t('navigation.devices'),
        [OG_DEVICES_ROUTE]: t('navigation.devices_og'),
    }

    return <Navigation routes={routes}/>
}