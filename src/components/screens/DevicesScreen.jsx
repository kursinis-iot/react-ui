import {
    Box,
    Button,
    Card,
    CardBody,
    Grid,
    Heading,
    Spinner,
    Text
} from "@chakra-ui/react";
import moment from "moment";

import {useEffect, useRef, useState} from "react";
import {deleteDevice as deleteDeviceApi} from "@/api/user-devices.js";
import {AddDeviceModal} from "../modals/AddDeviceModal.jsx";
import {DeviceCard} from "../DeviceCard.jsx";
import {connect, subscribe, unsubscribe} from "@/api/user-devices-ws.js";
import {Pagination} from "../Pagination.jsx";
import {ViewSensorHistoryModal} from "../modals/ViewSensorHistoryModal.jsx";
import {ConfigureDevicesModal} from "../modals/ConfigureDevicesModal.jsx";
import {useAccessToken} from "@kudze/auth-guard";
import {fetchDevicesPage_Old} from "@/repositories/devices.ts";

export function DevicesScreen() {
    const [devices, setDevices] = useState(undefined);
    const [subscribedDevices, setSubscribedDevices] = useState([]);
    const [{page, pageSize}, setPageInfo] = useState({page: 1, pageSize: 24});
    const [addingDevice, setAddingDevice] = useState(false);
    const [activeHistoryIndex, setActiveHistoryIndex] = useState(undefined);
    const [activeConfigureIndex, setActiveConfigureIndex] = useState(undefined);

    //const devicesRef = useRef();
    //devicesRef.current = devices === undefined ? undefined : {...devices};

    const loaded = devices !== undefined;

    const accessToken = useAccessToken();
    useEffect(() => {
        connect(accessToken);
    }, [accessToken]);

    function deleteDevice(uuid) {
        deleteDeviceApi(accessToken, uuid)
            .then(refreshListing);
    }

    function subscribeDevice(index) {
        const deviceUuid = devices.entries[index].uuid;
        const page = devices.page;
        setSubscribedDevices([
            ...subscribedDevices,
            deviceUuid
        ]);

        subscribe(
            deviceUuid,
            function (payload) {
                setDevices(devices => {
                    if (page !== devices.page)
                        return devices;

                    const newDevices = {
                        ...devices,
                        entries: [...devices.entries]
                    };

                    newDevices.entries[index] = {
                        ...devices.entries[index],
                        sensors: devices.entries[index].sensors.map(sensor => ({
                            ...sensor,
                            minute_of_logs: [
                                ...sensor.minute_of_logs,
                                {
                                    created_at: moment(parseInt(payload.timestamp)).utc(),
                                    device_sensor_uuid: sensor.uuid,
                                    data: payload[sensor.uuid],
                                    anomaly: false
                                }
                            ]
                        }))
                    }

                    return newDevices;
                });
            },
            function (timestamp) {
                const createdAt = moment(parseInt(timestamp)).utc();
                console.log(createdAt);

                setDevices(devices => {
                    if (page !== devices.page)
                        return devices;

                    return {
                        ...devices,
                        entries: devices.entries.map(
                            (device, _index) => index !== _index ? device : {
                                ...device,
                                sensors: device.sensors.map((sensor) => ({
                                    ...sensor,
                                    minute_of_logs: sensor.minute_of_logs.map((log) => !createdAt.isSame(log.created_at) ? log : ({
                                        ...log,
                                        anomaly: true
                                    }))
                                }))
                            }
                        )
                    };
                });
            },
            function (reason) {
                console.log(`Server disconnected ${deviceUuid}, reason: ${reason}!`);
                setSubscribedDevices(subscribedDevices.filter((_deviceUuid) => _deviceUuid !== deviceUuid));
            }
        );
    }

    function unsubscribeDevice(index) {
        const deviceUuid = devices.entries[index].uuid;
        unsubscribe(deviceUuid);

        const newSubscribedDevices = [...subscribedDevices];
        newSubscribedDevices.splice(newSubscribedDevices.indexOf(deviceUuid), 1);
        setSubscribedDevices(newSubscribedDevices);
    }

    function viewHistory(index) {
        setActiveHistoryIndex(index);
    }

    function configureDevice(index) {
        setActiveConfigureIndex(index);
    }

    function refreshListing() {
        fetchDevicesPage_Old(accessToken, page, pageSize, 'uuid', 'asc', ['sensors.minuteOfLogs', 'sensors.alerts.emailRecipients', 'anomalyModel'])
            .then((data) => setDevices(data));
    }

    useEffect(refreshListing, [page, pageSize]);

    function onDeviceAdded() {
        setAddingDevice(false);

        if (page === 1) {
            refreshListing();
            return;
        }

        setPageInfo({
            page: 1,
            pageSize: pageSize,
        });
    }

    function setPage(page) {
        setDevices(undefined);
        setPageInfo({
            page: page,
            pageSize: pageSize
        });
    }

    function onDeviceConfigured() {
        refreshListing();
        closeConfigureModal();
    }

    function closeConfigureModal() {
        setActiveConfigureIndex(undefined);
    }

    return <>
        <AddDeviceModal onClose={setAddingDevice.bind(this, false)} onCreate={onDeviceAdded} isOpen={addingDevice}/>
        <ViewSensorHistoryModal onClose={() => setActiveHistoryIndex(undefined)}
                                device={activeHistoryIndex === undefined ? undefined : devices.entries[activeHistoryIndex]}/>
        <ConfigureDevicesModal onClose={closeConfigureModal}
                               onSuccess={onDeviceConfigured}
                               device={activeConfigureIndex === undefined ? undefined : devices.entries[activeConfigureIndex]}/>

        <Card p={20} overflowY={"scroll"} w={"100%"} h={'100vh'}>
            <CardBody p={0} textAlign={"center"}>
                <Heading mb={10}>My devices</Heading>

                <Box>
                    {!loaded ? <Spinner size={"xl"}/> : <Devices
                        devices={devices.entries}
                        subscribedDevices={subscribedDevices}
                        page={devices.page}
                        maxPage={devices.aggregations.max_page}
                        deleteDevice={deleteDevice}
                        subscribeDevice={subscribeDevice}
                        unsubscribeDevice={unsubscribeDevice}
                        viewHistory={viewHistory}
                        configureDevice={configureDevice}
                        setPage={setPage}
                    />}
                </Box>

                <Button
                    colorScheme={"green"}
                    mt={10}
                    size={"lg"}
                    onClick={setAddingDevice.bind(this, true)}
                >
                    Add device
                </Button>
            </CardBody>
        </Card>
    </>
}

function Devices(
    {
        devices,
        subscribedDevices,
        page,
        maxPage,
        deleteDevice,
        subscribeDevice,
        unsubscribeDevice,
        viewHistory,
        configureDevice,
        setPage
    }
) {
    if (devices === undefined)
        return null;

    if (devices.length === 0)
        return <Text>At the moment you dont have any devices!</Text>

    function renderDeviceCard(device, index) {
        const subscribed = subscribedDevices.includes(device.uuid);

        return <DeviceCard
            key={index}
            device={device}
            actions={
                <>
                    {!subscribed ?
                        <Button flex='1' colorScheme={"green"} onClick={subscribeDevice.bind(this, index)}>
                            Subscribe
                        </Button> :
                        <Button flex='1' colorScheme={"red"} onClick={unsubscribeDevice.bind(this, index)}>
                            Unsubscribe
                        </Button>
                    }
                    <Button flex='1' colorScheme={'green'} onClick={viewHistory.bind(this, index)}>
                        History
                    </Button>
                    <Button flex='1' colorScheme={'yellow'} onClick={configureDevice.bind(this, index)}>
                        Configure
                    </Button>
                    <Button flex='1' colorScheme={'red'} onClick={deleteDevice.bind(this, device.uuid)}>
                        Delete
                    </Button>
                </>
            }
        />;
    }

    return <>
        <Pagination page={page} maxPage={maxPage} onChange={setPage}/>
        <Grid gridTemplateColumns={"1fr 1fr"} gap={10} maxW={"1200px"} mx={"auto"} my={10}>
            {devices.map(renderDeviceCard)}
        </Grid>
        <Pagination page={page} maxPage={maxPage}/>
    </>
}