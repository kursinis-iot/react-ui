import ReactDOM from 'react-dom/client'
import App from './app.tsx'

import "./i18n.ts";
import "./globals.css";

ReactDOM.createRoot(document.getElementById('root')!).render(<App/>)
