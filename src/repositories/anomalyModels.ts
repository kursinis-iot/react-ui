import {request} from "@/api/rest.ts";

export type AnomalyModel = {
    uuid: string,
    name: string,
    created_at: string,
    updated_at: string
}

export type AnomalyModelsFetchAllResult = AnomalyModel[];

export async function fetchAnomalyModels(
    token: string
): Promise<AnomalyModelsFetchAllResult> {
    return await request(`anomalies/models`, "GET", token);
}