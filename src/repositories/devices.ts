import {OrderByDirectionType, request} from "@/api/rest.ts";

export type DeviceSensorLog = {
    created_at: string,
    data: any
    device_sensor_uuid: string,
    uuid: string
}

export type DeviceSensorAlertEmailRecipient = {
    created_at: string,
    device_sensor_alert_uuid: string,
    email: string,
    updated_at: string,
    uuid: string
}

export type DeviceSensorAlertType = 'range' | 'downtime';

export type DeviceSensorAlert = {
    created_at: string,
    device_sensor_uuid: string,
    email_recipients: DeviceSensorAlertEmailRecipient[],
    interval: number,
    payload: any,
    title: string,
    type: DeviceSensorAlertType,
    updated_at: string,
    uuid: string
}

export type DeviceSensor = {
    alerts?: DeviceSensorAlert[],
    created_at: string,
    device_uuid: string,
    jsonpath_query: string,
    minute_of_logs: DeviceSensorLog[],
    title: string,
    updated_at: string,
    uuid: string
}

export type DeviceStatus = "allocating" | "allocated" | "online" | "offline";

export type Device = {
    added_by_user: string | null,
    config: any,
    created_at: string,
    last_sensor_updated_at: string | null,
    sensors?: DeviceSensor[]
    status: DeviceStatus,
    title: string,
    toggle_offline_after_seconds: number,
    updated_at: string,
    uuid: string
}

export type PaginatedLinkType = {
    url: string | null,
    label: string,
    active: boolean
}

export type DevicesPageResponseType = {
    current_page: number,
    data: Device[],
    first_page_url: string,
    from: number,
    last_page: number,
    last_page_url: string,
    links: PaginatedLinkType[],
    next_page_url: string | null,
    path: string,
    per_page: number,
    prev_page_url: string | null,
    to: number,
    total: number
}

export type OrderByDeviceType = 'uuid'
    | 'title'
    | 'last_sensor_updated_at'
    | 'status'
    | 'created_at'
    | 'updated_at';

export type DeviceAssociationType = "sensors"
    | "sensors.minuteOfLogs"
    | "sensors.alerts"
    | "sensors.alerts.emailRecipients";

export async function fetchDevicesPage(
    token: string,
    page: number,
    pageSize: number = 25,
    orderBy: OrderByDeviceType = "uuid",
    orderByDirection: OrderByDirectionType = "asc",
    associations: DeviceAssociationType[] = undefined
): Promise<DevicesPageResponseType> {
    const queryArgs = new URLSearchParams({
        page: page.toString(),
        pageSize: pageSize.toString(),
        orderBy: orderBy,
        orderByDirection: orderByDirection
    });

    if (associations !== undefined)
        associations.forEach((association, index) => queryArgs.append(`with[${index}]`, association));

    return await request(`devices?${queryArgs}`, "GET", token);
}

export type OldAggregationsType = {
    max_page: number,
    total: number
}

export type DevicesPageOldResponseType = {
    page: number
    entries: Device[]
    aggregations: OldAggregationsType
}

//TODO: remove
export async function fetchDevicesPage_Old(
    token: string,
    page: number,
    pageSize: number = 25,
    orderBy: OrderByDeviceType = "uuid",
    orderByDirection: OrderByDirectionType = "asc",
    associations: DeviceAssociationType[] = undefined
): Promise<DevicesPageOldResponseType> {
    const result = await fetchDevicesPage(token, page, pageSize, orderBy, orderByDirection, associations);

    return {
        page: result.current_page,
        entries: result.data,
        aggregations: {
            max_page: result.last_page,
            total: result.total
        }
    }
}