import {BrowserRouter, Route, Routes} from "react-router-dom";
import {AuthGuard} from "@kudze/auth-guard";
import {FunctionComponent} from "react";
import {HomeRoute} from "@/routes/home.tsx";
import {DevicesRoute} from "@/routes/devices.tsx";
import {DevicesOldRoute} from "@/routes/devices_old.tsx";

function ProtectedRoute(path: string, Element: FunctionComponent) {
    return <Route path={path} element={makeRouteElement(Element)}/>;
}

function makeRouteElement(Element: FunctionComponent) {
    return <AuthGuard><Element/></AuthGuard>;
}

export const HOME_ROUTE = '/';
export const DEVICES_ROUTE = '/devices';

//TODO: remove
export const OG_DEVICES_ROUTE = '/devices_og';

export function Router() {
    return <BrowserRouter>
        <Routes>
            {ProtectedRoute(HOME_ROUTE, HomeRoute)}
            {ProtectedRoute(DEVICES_ROUTE, DevicesRoute)}
            {ProtectedRoute(OG_DEVICES_ROUTE, DevicesOldRoute)}
        </Routes>
    </BrowserRouter>
}