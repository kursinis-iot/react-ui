import {ReactNode, useState} from "react";
import {cn, GlobalBreadcrumbNode, ProtectedPage, Button, Card, CardHeader} from "@kudze/auth-guard";
import {LucideIcon, Menu} from "lucide-react";
import {RootNavigation} from "@/components/navigation/rootNavigation.tsx";

export type OverlayProps = {
    onlyPhoneVisible?: boolean
};

export type Overlay = JSX.Element;

export function makeOverlay(
    Icon: LucideIcon,
    onClick: () => void,
    {
        onlyPhoneVisible = false
    }: OverlayProps = {}
): Overlay {
    const buttonClassNames = cn('mt-2 w-[30px] h-[30px] p-0', {
        'inline-flex md:hidden': onlyPhoneVisible
    });

    return <Button variant="default" className={buttonClassNames} onClick={onClick}>
        <Icon className="h-[20px] w-[20px]"/>
    </Button>
}

export type BaseRouteProps = {
    children: ReactNode,
    overlays?: Overlay[],
    breadcrumb?: GlobalBreadcrumbNode[]
};

export function BaseRoute(
    {
        children,
        overlays = [],
        breadcrumb = []
    }: BaseRouteProps
) {
    const [mobileExpanded, setMobileExpanded] = useState<boolean>(false);

    overlays = [
        ...overlays,
        makeOverlay(Menu, () => setMobileExpanded(!mobileExpanded), {
            onlyPhoneVisible: true
        })
    ];

    const navCardClassName = cn('fixed md:static md:w-max root-navigation transition-all md:transition-none ml-0', {
        'opacity-0 md:opacity-100 pointer-events-none md:pointer-events-auto -ml-1 md: ml-0': !mobileExpanded,
    })

    return <ProtectedPage containerClass="root-container p-0" breadcrumb={breadcrumb}>
        <Card className={navCardClassName}>
            <CardHeader className={"flex flex-col gap-2"}>
                <RootNavigation/>
            </CardHeader>
        </Card>
        <Card className={"w-full min-h-max md:ml-2.5"}>
            {children}
        </Card>
        <div className={"root-overlays"}>
            {overlays.map((overlay, key) => <div key={key}>{overlay}</div>)}
        </div>
    </ProtectedPage>;
}