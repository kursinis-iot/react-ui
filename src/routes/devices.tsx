import {useTranslation} from "react-i18next";
import {GlobalBreadcrumbNode} from "@kudze/auth-guard";
import {DEVICES_ROUTE} from "@/router.tsx";
import {BaseRoute} from "@/routes/baseRoute.tsx";

export function DevicesRoute() {
    const {t} = useTranslation();

    const devicesBreadcrumb: GlobalBreadcrumbNode = {
        href: DEVICES_ROUTE,
        label: t('navigation.devices')
    }

    return <BaseRoute breadcrumb={[devicesBreadcrumb]}>
        <div className="p-5">TODO</div>
    </BaseRoute>
}