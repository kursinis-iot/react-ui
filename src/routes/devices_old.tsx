import {useTranslation} from "react-i18next";
import {GlobalBreadcrumbNode} from "@kudze/auth-guard";
import {OG_DEVICES_ROUTE} from "@/router.tsx";
import {BaseRoute} from "@/routes/baseRoute.tsx";
import {ChakraProvider} from "@chakra-ui/react";
import {DevicesScreen} from "@/components/screens/DevicesScreen.jsx"

export function DevicesOldRoute() {
    const {t} = useTranslation();

    const devicesBreadcrumb: GlobalBreadcrumbNode = {
        href: OG_DEVICES_ROUTE,
        label: t('navigation.devices_og')
    }

    return <BaseRoute breadcrumb={[devicesBreadcrumb]}>
        <ChakraProvider>
            <DevicesScreen/>
        </ChakraProvider>
    </BaseRoute>
}