import {BaseRoute} from "@/routes/baseRoute.tsx";
import {useTranslation} from "react-i18next";
import {GlobalBreadcrumbNode} from "@kudze/auth-guard";
import {HOME_ROUTE} from "@/router.tsx";

export function HomeRoute() {
    const {t} = useTranslation();

    const homeBreadcrumb: GlobalBreadcrumbNode = {
        href: HOME_ROUTE,
        label: t('navigation.home')
    }

    return <BaseRoute breadcrumb={[homeBreadcrumb]}>
        <div className={"p-5"}>TODO</div>
    </BaseRoute>
}